# gw_memory_project

Gravitational waves passing through a region of spacetime leave behind a permanent distortion, with strain typically on the order of one tenth the peak strain of the wave—the so-called memory effect. Linear and nonlinear components exist in gravitational wave memory, the latter appearing as a non-oscillatory, cumulative signal. Current gravitational wave detectors have not yet been able to reliably detect and isolate this low-frequency, nonlinear component which skews the numerical inferences of gravitational wave source parameters. Because this effect is cumulative, it is non-negligible, and its non-oscillatory nature distinguishes it from the rest of the waveform, making it detectable, in theory. Though previous studies have quantified and suggested improvements for the detectability of nonlinear memory, more templates and new data are available than ever before. In this project, we apply Bayesian parameter estimation to simulated gravitational waves from compact binary coalescences with memory to determine nonlinear memory detectability.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

lalsuite
bilby
gwmemory
gwpy

```
example
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
example
```

repeat

```
example
```

example of how to run the program once installed

## Extracting the memory parameter

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
example
```

### And coding style tests

Explanation of an inference run

```
example
```

## Deployment

additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Darin C. Mumma** - *Initial work* - [Mumma-project-data](https://git.ligo.org/darin.mumma/Mumma-project-data)

See also the list of [contributors](https://git.ligo.org/darin.mumma/Mumma-project-data/contributors) who participated in this project.

## Acknowledgments

* Dr. Colt Talbot - gwmemory and insight for this program
* Alvin Li - insight and coauthorship of program
* etc.

